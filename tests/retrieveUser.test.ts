import { expect } from 'chai';
import { retrieveUser } from '../ts_Modules/retrieveUser';

describe('retrieve user Details',()=>{
    it('should retrieve user',async ()=>{
      let result:any = await retrieveUser('Rushikesh');
      expect(result.username).to.equal('Rushikesh');
    })

    it('should not retrieve user',async ()=>{
        let result:any = await retrieveUser('Pranav');
        expect(result).to.equal('No data available');
      })
})
