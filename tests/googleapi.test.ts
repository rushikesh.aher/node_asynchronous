import { expect } from 'chai';
import { getLocation }from '../ts_Modules/googleapi'

describe('google api ',()=>{
    it('should get latitute longitude', async ()=>{
      let result:any = await getLocation('Mumbai');
      console.log(result);
      expect(result).to.equal('{"lat":18.95238,"lng":72.832711}');
    })

    // it('should return empty',async ()=>{
    //     let result:any =await getLocation('');
    //     expect(result).to.equal('');
    //   })
})